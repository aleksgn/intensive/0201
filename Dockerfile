FROM python:3-alpine
ENV user=admin
ENV email=admin@example.com
ENV password=pass

WORKDIR /app
COPY requirements.txt /app/
RUN pip3 install -r requirements.txt \
    && mkdir db
COPY . .
EXPOSE 8000
CMD sh ./init.sh && python3 manage.py runserver 0.0.0.0:8000
